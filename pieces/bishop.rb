# -*- coding: utf-8 -*-
require_relative './sliding_pieces.rb'

class Bishop < SlidingPieces
  def initialize(color, pos, board)
    super
  end

  def symbol
    { black: '♝', white: '♗'  }
  end

  def move_dirs
    [[1, 1], [-1, 1], [1, -1], [-1, -1]]
  end
end
