# -*- coding: utf-8 -*-
require_relative './piece.rb'

class Pawn < Piece
  def initialize(color, pos, board)
    super
  end

  def symbol
    { black: '♟', white: '♙' }
  end

  def moves
    killing_moves(@color) + vertical_moves(@color)
  end

  def killing_moves(color)
    possible_moves = []

    row_step = (color == :white ? 1 : -1)
    dirs = [[row_step, -1], [row_step, 1]]

    dirs.each do |dir|
      d1, d2 = dir
      new_pos = [@pos[0] + d1, @pos[1] + d2]

      possible_moves << new_pos if self.blocked_by_opp?(new_pos)
    end

    possible_moves
  end

  def vertical_moves(color)
    possible_moves = []

    row_step = (color == :white ? -1 : 1)

    distance = 1

    new_pos = [@pos[0], @pos[1]]

    if (color == :white && @pos[0] == 6) || (color == :black && @pos[0] == 1)
      distance += 1
    end

    counter = 0
    while counter < distance
      new_pos = [new_pos[0] + row_step, new_pos[1]]
      break unless in_bound?(new_pos)
      break unless @board[new_pos].nil?
      # break if self.blocked_by_self?(new_pos)
      # break if self.blocked_by_opp?(new_pos)

      possible_moves << new_pos

      counter += 1
    end

    possible_moves
  end
end
