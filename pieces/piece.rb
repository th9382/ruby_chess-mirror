class Piece
  attr_reader :symbol, :color
  attr_accessor :pos

  def initialize(color, pos, board)
    @color = color
    @pos = pos
    @board = board

    @board.add_piece(pos, self)
  end

  def symbol
    fail NotImplementedError
  end

  def render
    symbol[color]
  end

  def move_into_check?(pos)
    board_dup = @board.dup
    board_dup.move!(@pos, pos)

    board_dup.in_check?(@color)
  end

  def valid_moves
    moves.reject { |move| move_into_check?(move) }
  end

  def in_bound?(pos)
    x, y = pos
    x >= 0 && x < 8 && y >= 0 && y < 8
  end

  def blocked_by_self?(pos)
    !@board.empty?(pos) && @board[pos].color == color
  end

  def blocked_by_opp?(pos)
    !@board.empty?(pos) && @board[pos].color != color
  end

  def inspect
    "color: #{@color}, symbol: #{symbol}"
  end
end
