# -*- coding: utf-8 -*-
require_relative './stepping_pieces.rb'

class Knight < SteppingPieces
  def initialize(color, pos, board)
    super
  end

  def symbol
    { black: '♞' , white: '♘' }
  end

  def move_dirs
    [[1, -2], [2, -1], [1, 2], [2, 1],
    [-1, -2], [-2, -1], [-2, 1], [-1, 2]]
  end
end
