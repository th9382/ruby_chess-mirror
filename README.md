# Chess
Classic 2-player chess game written in ruby (runs in terminal). Strong focus on inheritance and polymorphism by subclassing each chess piece. Also utilized unicode to display proper chess symbols and the colorize gem to add some color to the otherwise bland terminal window.

## Directions
- Load game.rb in IRB
- Create a new game => game = Game.new
- And then play the game => game.play
- Select a start and end position to make a move. Error checking ensures you can only make valid moves.

## Todo
- support special rules such as En passant
- convert to browser-based game for more pleasant UI.

## Screenshot
![ss]

[ss]: ./images/chess.png
